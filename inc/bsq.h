
#ifndef BSQ_H

# define BSQ_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <fcntl.h>
# include <libft.h>

# define BUFF_SIZE		512

# define USAGE			"[files ...]\n"
# define SYNTAX_ERROR	"syntax error\n"
# define PARSE_ERROR	"parse_error\n"
# define MALLOC_ERROR	"malloc error\n"
# define FILE_ERROR		"file error\n"
# define READ_ERROR		"read error\n"
# define CLOSE_ERROR	"close error\n"

typedef	struct s_file	t_file;
typedef	struct s_line	t_line;

struct					s_file
{
	char				*name;
	int					fd;
	t_line				*lines;
	char				*chars;
	int					nb;
	int					x;
	int					y;
	int					size;
};

struct					s_line
{
	char				*line;
	t_line				*next;
};

void					parse_file(t_file *f);
void					parse_args(int ac, char **av);
int						ft_getline(int fd, char **line);

void					print_error(char *error);
void					print_result(t_file *f);

void					search_print(t_file *f);

t_line					*new_line(char *line);
t_file					*new_file(int fd, char *name);
void					add_line(t_line **base, t_line *line);
void					free_file(t_file *f);

#endif
