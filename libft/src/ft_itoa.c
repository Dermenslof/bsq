/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/24 09:49:42 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:50:38 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char			*ft_itoa(int n)
{
	int			i;
	int			div;
	int			d;
	char		*str;

	div = 1;
	d = n < 0 ? 1 : 0;
	i = n < 0 ? 1 : 0;
	n = n > 0 ? -n : n;
	while (n / div < -9 && ++d)
		div *= 10;
	str = (char *)malloc(sizeof(char) * (d + 1));
	str[0] = n < 0 ? '-' : '0' - n / div % 10;
	while (div)
	{
		str[i++] = '0' - n / div % 10;
		div /= 10;
	}
	str[i] = '\0';
	return (str);
}
