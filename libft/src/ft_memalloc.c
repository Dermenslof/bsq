/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 08:47:09 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:51:08 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>

void				*ft_memalloc(size_t n)
{
	unsigned char	*mem;
	size_t			i;

	i = 0;
	if (n == 0)
		return (NULL);
	mem = (unsigned char *)malloc(sizeof(unsigned char) * n);
	if (!mem)
		return (NULL);
	while (i < n)
		mem[i++] = 0;
	return (mem);
}
