/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 11:04:05 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:53:42 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char			*ft_strmap(char const *s, char (*f)(char))
{
	int			i;
	char		*tmp;

	i = -1;
	tmp = (char *)malloc(sizeof(char) * (ft_strlen(s) + 1));
	while (s[++i])
		tmp[i] = f(s[i]);
	tmp[i] = '\0';
	return (tmp);
}
