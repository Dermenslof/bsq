/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 16:57:20 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:53:32 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <string.h>

/*
 *size_t			ft_strlen(const char *s)
 *{
 *    size_t		i;
 *
 *    i = 0;
 *    while (*s)
 *    {
 *        i++;
 *        s++;
 *    }
 *    return (i);
 *}
 */
size_t			ft_strlen(const char *s)
{
	size_t		i;
	uint64_t	hack;

	i = 0;
	while (1)
	{
		hack = *((uint64_t *)(s + i));
		if (!(hack & 0xFF))
			return (i);
		if (!(hack & 0xFF00))
			return (i + 1);
		if (!(hack & 0xFF0000))
			return (i + 2);
		if (!(hack & 0xFF000000))
			return (i + 3);
		if (!(hack & 0xFF00000000))
			return (i + 4);
		if (!(hack & 0xFF0000000000))
			return (i + 5);
		if (!(hack & 0xFF000000000000))
			return (i + 6);
		if (!(hack & 0xFF00000000000000))
			return (i + 7);
		i += 8;
	}
	return (i);
}
