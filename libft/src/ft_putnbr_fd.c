/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 16:28:33 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:52:23 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void				ft_putnbr_rec_fd(int n, int fd)
{
	if (n < -9)
		ft_putnbr_rec_fd(n / 10, fd);
	ft_putchar_fd('0' - n % 10, fd);
}

void					ft_putnbr_fd(int n, int fd)
{
	if (n < 0)
		ft_putchar_fd('-', fd);
	else
		n = -n;
	ft_putnbr_rec_fd(n, fd);
}
