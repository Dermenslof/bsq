/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 23:56:31 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:54:20 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strstr(const char *s1, const char *s2)
{
	size_t		i;
	size_t		j;

	i = 0;
	if (!*s2 || !s2)
		return ((char *)s1);
	while (s1[i])
	{
		if (s1[i] == s2[0])
		{
			j = 1;
			while (s2[j])
			{
				if (s1[i + j] != s2[j])
					break ;
				j++;
			}
			if (!s2[j])
				return ((char *)&(s1[i]));
		}
		i++;
	}
	return (NULL);
}
