/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 23:10:07 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:54:15 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int			ft_count_word(char const *s, char c, char i)
{
	if (!*s)
		return (0);
	if (*s == c || (i && *(s - 1) != c))
		return (ft_count_word(++s, c, ++i));
	return (1 + ft_count_word(++s, c, ++i));
}

static int			ft_count_char(char const *s, char c)
{
	if (!*s || *s == c)
		return (0);
	return (1 + ft_count_char(++s, c));
}

static char		**ft_clean(char **tab, int size)
{
	int		i;

	i = -1;
	while (++i < size)
	{
		if (tab[i])
			free(tab[i]);
	}
	free(tab);
	return (NULL);
}

char		**ft_strsplit(char const *s, char c)
{
	int		i;
	int		j;
	char	**tab;

	if (!s)
		return (NULL);
	if (!(tab = malloc(sizeof(char *) * (ft_count_word(s, c, 0) + 1))))
		return (NULL);
	i = 0;
	while (*s)
	{
		while (*s == c)
			++s;
		if (!*s)
			break ;
		if (!(tab[i] = malloc(ft_count_char(s, c) + 1)))
			return (ft_clean(tab, i - 1));
		j = 0;
		while (*s && *s != c)
			tab[i][j++] = *s++;
		tab[i++][j] = '\0';
	}
	tab[i] = NULL;
	return (tab);
}
