/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 15:28:24 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:52:19 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void				ft_putnbr_rec(int n)
{
	if (n < -9)
		ft_putnbr_rec(n / 10);
	ft_putchar('0' - n % 10);
}

void					ft_putnbr(int n)
{
	if (n < 0)
		ft_putchar('-');
	else
		n = -n;
	ft_putnbr_rec(n);
}
