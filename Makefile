# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/06 17:08:33 by fmontaro          #+#    #+#              #
#    Updated: 2015/01/09 14:20:12 by fmontaro         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		= bsq

SHELL		= /bin/zsh
CC			= clang
INCS		= -Iinc -Ilibft/inc
CFLAGS		= -Ofast -g0 -march=native -mtune=native -pedantic -W -Werror -Wextra -Wall
LIBS		= -L./libft -lft

FILES		= main.c				\
			  parse.c				\
			  output.c				\
			  file.c				\
			  line.c				\
			  search.c

SRC			= $(addprefix src/, $(FILES))
OBJ			= $(SRC:src/%.c=obj/%.o)

.SILENT: all $(NAME) clean fclean re dirobj clibft

all: dirobj clibft $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LIBS)
	echo "\033[33;3m$(NAME) \033[32;2m\t\t\tCreated\033[0m"

obj/%.o: src/%.c
	@$(CC) $(INCS) $(CFLAGS) -o $@ -c $^

clean:
	make clean -C libft --no-print-directory
	test ! -d obj || \
		echo "\033[31;4mDelete:\033[0m\033[32;2m\t\t\tobj/\033[0m" && \
		rm -rf obj

fclean: clean
	make fclean -C libft --no-print-directory
	test ! -f $(NAME) || \
		echo "\033[31;4mDelete:\033[0m\033[32;2m\t\t\t$(NAME)\033[0m" && \
		rm -f $(NAME)

re: fclean all

clibft:
	make re -C libft --no-print-directory

dirobj:
	test -d obj || \
		echo "\033[33;4mCreate:\033[0m\033[32;2m\t\t\tobj/\033[0m" && \
		mkdir -p obj

