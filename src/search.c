
#include "bsq.h"

void			convert(int *tab, char *str, char *chars, size_t len)
{
	size_t		i;

	i = -1;
	while (++i < len)
		tab[i] = str[i] == *chars ? 1 : 0;
}

int				get_min(int **tab, int x)
{
	int			a;
	int			b;
	int			c;

	a = tab[0][x];
	b = x ? tab[0][x - 1] : 0;
	c = x ? tab[1][x - 1] : 0;
	if (a <= b && a <= c)
		return (a);
	if (b <= a && b <= c)
		return (b);
	return (c);
}

void			process(int **tab, int y, int len, t_file *f)
{
	int			x;

	x = -1;
	while (++x < len)
	{
		if (y && tab[1][x])
			tab[1][x] = get_min(tab, x) + 1;
		if (tab[1][x] > f->size)
		{
			f->size = tab[1][x];
			f->x = x - f->size + 1;
			f->y = y - f->size + 1;
		}
	}
	x = -1;
	while (++x < len)
		tab[0][x] = tab[1][x];
}

void			search_print(t_file *f)
{
	static size_t	len = 0;
	int				**tab;
	int				y;
	t_line			*line;

	y = 0;
	if (!len)
		len = ft_strlen(f->lines->line);
	tab = malloc(sizeof(int *) * 2);
	tab[0] = malloc(sizeof(int) * len);
	tab[1] = malloc(sizeof(int) * len);
	line = f->lines;
	while (line)
	{
		convert(tab[1], line->line, f->chars, len);
		process(tab, y++, (int)len, f);
		line = line->next;
	}
	free(tab[0]);
	free(tab[1]);
	free(tab);
	print_result(f);
}
