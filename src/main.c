
#include "bsq.h"

int			main(int ac, char **av)
{
	if (ac < 2)
		parse_file(new_file(0, "stdin"));
	else
		parse_args(ac, av);
	return (0);
}
