
#include "bsq.h"

void			print_error(char *error)
{
	ft_putstr(error);
}

static void		set_lines(t_file *f, char *str, int len)
{
	int			x;
	int			y;
	t_line		*l;

	y = -1;
	l = f->lines;
	while (l && (++y > -1))
	{
		x = -1;
		while (++x < len - 1)
		{
			if (f->size > 0 && y >= f->y && y < f->y + f->size
					&& x >= f->x && x < f->x + f->size)
				str[y * len + x] = f->chars[2];
			else
				str[y * len + x] = l->line[x];
		}
		str[y * len + x] = '\n';
		l = l->next;
	}
	str[y * len + x + 1] = 0;
}

void			print_result(t_file *f)
{
	static int	count = 0;
	char		*str;
	size_t		len;

	len = ft_strlen(f->lines->line) + 1;
	str = malloc(len * f->nb + 1);
	set_lines(f, str, (int)len);
	if (count++)
		ft_putchar('\n');
	ft_putstr(f->name);
	ft_putstr(":\n");
	ft_putstr(str);
	free(str);
}
