
#include "bsq.h"

t_line			*new_line(char *str)
{
	t_line		*line;

	if ((line = malloc(sizeof(t_line))))
	{
		line->next = NULL;
		line->line = ft_strdup(str);
	}
	return (line);
}

t_file			*new_file(int fd, char *name)
{
	t_file		*file;

	if ((file = malloc(sizeof(t_file))))
	{
		file->chars = NULL;
		file->lines = NULL;
		file->fd = fd;
		file->name = name;
		file->nb = 0;
		file->x = 0;
		file->y = 0;
		file->size = 0;
	}
	return (file);
}

void			add_line(t_line **base, t_line *line)
{
	t_line		*tmp;

	if (!*base)
	{
		*base = line;
		return ;
	}
	tmp = *base;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = line;
}

static void		free_lines(t_line *l)
{
	if (l->next)
		free_lines(l->next);
	free(l->line);
	free(l);
}

void			free_file(t_file *f)
{
	free(f->chars);
	free_lines(f->lines);
	free(f);
}
