
#include "bsq.h"

static char	get_infos(t_file *f, char *l)
{
	char	error;

	error = 0;
	f->nb = ft_atoi(l);
	if (f->nb < 1)
		++error;
	while (*l >= '0' && *l <= '9')
		++l;
	if (ft_strlen(l) != 3 || (*l == l[1] || *l == l[2] || l[1] == l[2]))
		++error;
	f->chars = ft_strdup(l);
	if (error)
		print_error(PARSE_ERROR);
	return (!error);
}

static char	check_line(char *chars, char *line)
{
	while (*line)
	{
		if (*line != *chars && *line != chars[1])
			return (0);
		++line;
	}
	return (1);
}

static char	parse_line(t_file *f, char *line, int i)
{
	static size_t	len = 0;

	if (!i)
		return (get_infos(f, line));
	else
	{
		if (i == 1)
			len = ft_strlen(line);
		else if (len != ft_strlen(line) || !len)
			return (0);
		else if (!check_line(f->chars, line))
		{
			print_error(SYNTAX_ERROR);
			return (0);
		}
		add_line(&f->lines, new_line(line));
	}
	return (1);
}

void		parse_file(t_file *f)
{
	int		ret;
	char	*line;
	int		i;

	i = -1;
	line = NULL;
	while ((ret = ft_getline(f->fd, &line)) && line)
	{
		if (ret < 0)
		{
			print_error(READ_ERROR);
			break ;
		}
		if (!parse_line(f, line, ++i) && (ret = -1))
			break ;
		free(line);
		line = NULL;
	}
	if (line)
		free(line);
	if (i != f->nb)
		print_error(PARSE_ERROR);
	else if (ret > -1 && i > 0)
		search_print(f);
	free_file(f);
}

void		parse_args(int ac, char **av)
{
	int		fd;
	int		i;

	i = 0;
	while (++i < ac)
	{
		if ((fd = open(av[i], O_RDONLY)) < 0)
		{
			print_error(READ_ERROR);
			continue ;
		}
		parse_file(new_file(fd, av[i]));
		if (close(fd) < 0)
			print_error(CLOSE_ERROR);
	}
}
